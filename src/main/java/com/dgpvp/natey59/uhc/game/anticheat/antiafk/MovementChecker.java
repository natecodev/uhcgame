/**
	@author natey59
*/

package com.dgpvp.natey59.uhc.game.anticheat.antiafk;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import com.dgpvp.natey59.uhc.game.UHCGame;
import com.dgpvp.natey59.uhc.game.anticheat.utils.SerializedLocation;
import com.dgpvp.natey59.uhc.messenger.PlayerMessenger;

public class MovementChecker extends BukkitRunnable {

	private UUID player;
	private SerializedLocation lastKnownLocation;
	private UHCGame game;
	
	public MovementChecker(UHCGame game, Player player) {
		this.player = player.getUniqueId();
		this.lastKnownLocation = new SerializedLocation(player.getLocation());
		this.game = game;
	}
	
	@Override
	public void run() {		
		try {
			SerializedLocation currentLocation = new SerializedLocation(Bukkit.getPlayer(player).getLocation());
			
			if (game.getGameState().allowsDamage()) {
				if (lastKnownLocation.equals(currentLocation) ) {
					
					game.addAfkPlayer(Bukkit.getPlayer(player));
					PlayerMessenger.errorMessage(Bukkit.getPlayer(player), game.getLangHandler().getString("message.anticheat.afkToLong"));
					
				} else {
					lastKnownLocation = currentLocation;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	
	
}
