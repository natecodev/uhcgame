/**
	@author natey59
*/

package com.dgpvp.natey59.uhc.game.runnables;

import org.bukkit.scheduler.BukkitRunnable;

import com.dgpvp.natey59.uhc.game.UHCGame;

public class Teleporter extends BukkitRunnable {

	private UHCGame game;
	
	public Teleporter(UHCGame game) {
		this.game = game;
	}
	
	@Override
	public void run() {
		if (game.getTeleportQueue().hasNext()) {
			game.getTeleportQueue().teleportNext();
		}
		
	}

}
