/**
	@author natey59
*/

package com.dgpvp.natey59.uhc.game.runnables;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import com.dgpvp.natey59.uhc.game.UHCGame;
import com.dgpvp.natey59.uhc.status.GameState;

public class ScatterPlayers extends BukkitRunnable {

	private UHCGame plugin;
	
	public ScatterPlayers(UHCGame instance) {
		this.plugin = instance;
	}
	
	public void run() {
		
		for (Player player : Bukkit.getOnlinePlayers()) {
			
			player.setHealth(20);
			player.getPlayer().setSaturation(20);
			player.getPlayer().setFoodLevel(20);
			player.getPlayer().getInventory().clear();
			
		}
		
		plugin.setGameState(GameState.PRE_GAME);
				
		for (Player player : Bukkit.getOnlinePlayers()) {
			
			if (UHCGame.scatterMap.hasNext()) {
				Location loc = UHCGame.scatterMap.getNext();
				
				loc.setY(UHCGame.getGameWorld().getHighestBlockYAt(loc));
				
				plugin.addTeleport(player, loc);
			} else {
				plugin.returnPlayerToLobby(player, "Some how a spawn point was not generated for you. Please contact Natey59.");
			}
			
		}
		
		BukkitRunnable teleport = new Teleporter(plugin);
		
		int teleportRunnableId = Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin, teleport, 0, 2);
		
		Bukkit.getScheduler().cancelTask(teleportRunnableId);
		
		BukkitRunnable runGame = new RunGame(plugin);
		
		runGame.runTaskLaterAsynchronously(plugin, 300);
	}
	
}
