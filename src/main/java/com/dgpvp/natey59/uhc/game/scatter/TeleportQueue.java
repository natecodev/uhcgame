/**
	@author natey59
*/

package com.dgpvp.natey59.uhc.game.scatter;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.entity.Player;

public class TeleportQueue {

	private List<TeleportEntry> teleports = new ArrayList<TeleportEntry>();
	
	public void queueTeleport(Player player, Location location) {
		this.teleports.add(new TeleportEntry(player, location));
	}
	
	public void teleportNext() {
		
		if (this.hasNext()) {
			teleports.get(0).getPlayer().teleport(teleports.get(0).getLocation());
		}
		
	}
	
	public boolean hasNext() {
		boolean hasNext = false;
		
		if (this.teleports.size() > 0) {
			hasNext = true;
		}
		
		return hasNext;
		
	}
	
	class TeleportEntry {
		
		private Player player;
		private Location location;
		
		public TeleportEntry(Player player, Location location) {
			this.player = player;
			this.location = location;
		}
		
		public Player getPlayer() {
			return this.player;
		}
		
		public Location getLocation() {
			return this.location;
		}
		
	}
	
}
