/**
	@author natey59
*/

package com.dgpvp.natey59.uhc.game.runnables;

import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitRunnable;

import com.dgpvp.natey59.uhc.game.UHCGame;
import com.dgpvp.natey59.uhc.messenger.PlayerMessenger;
import com.wimbli.WorldBorder.Config;

public class GenerateBorder extends BukkitRunnable {

	private UHCGame game;
	
	public GenerateBorder(UHCGame game) {
		this.game = game;
	}
	
	@Override
	public void run() {
		
		
		Config.setBorder(UHCGame.getGameWorld().getName(), game.getMapRadius(), game.getMapRadius(), 0, 0);
		
		//game.getScatterUtils().preloadScatterPositions();
		
		try {
			PlayerMessenger.broadcast("3...");
			Thread.sleep(1000);
			PlayerMessenger.broadcast("2...");
			Thread.sleep(1000);
			PlayerMessenger.broadcast("1...");;
			Thread.sleep(100);
			PlayerMessenger.broadcast(game.getLangHandler().getString("message.broadcast.everyoneWillBeTeleported"));
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
//		BukkitRunnable generateScatterPoints = new GenerateScatterPoints(game);
//		
//		Bukkit.getScheduler().scheduleSyncDelayedTask(game, generateScatterPoints, 0);
		BukkitRunnable scatterPlayers = new ScatterPlayers(game);
		
		Bukkit.getScheduler().scheduleSyncDelayedTask(game, scatterPlayers, 0);
		
	}
}
