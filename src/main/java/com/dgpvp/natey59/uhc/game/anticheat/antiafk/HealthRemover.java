/**
	@author natey59
*/

package com.dgpvp.natey59.uhc.game.anticheat.antiafk;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import com.dgpvp.natey59.uhc.game.UHCGame;

public class HealthRemover extends BukkitRunnable {

	private UHCGame game;
	
	public HealthRemover(UHCGame game) {
		this.game = game;
	}
	
	
	//Deplate player health every 4 seconds they are AFK.
	@Override
	public void run() {
		
		for (UUID id : game.getAfkPlayers()) {
			
			Player player = Bukkit.getPlayer(id);
			
			player.setHealth(player.getHealth()-1);
			
		}
		
	}

}
