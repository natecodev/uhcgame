/**
	@author natey59
*/

package com.dgpvp.natey59.uhc.game.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

import com.dgpvp.natey59.uhc.game.UHCGame;
import com.dgpvp.natey59.uhc.player.UHCPlayer;

public class OnPlayerQuit implements Listener {

	private UHCGame game;
	
	public OnPlayerQuit(UHCGame game) {
		this.game = game;
	}
	
	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent e) {
				
		game.getServerData().setPlayerCount(game.getPlayerCount() - 1);
		
		game.removePlayer(new UHCPlayer(e.getPlayer()));
	}
	
}
