/**
	@author natey59
*/

package com.dgpvp.natey59.uhc.game.listeners;

import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.PlayerDeathEvent;

import com.dgpvp.natey59.uhc.game.UHCGame;
import com.dgpvp.natey59.uhc.player.UHCPlayer;

public class OnPlayerDeath implements Listener {

	private UHCGame game;

	public OnPlayerDeath(final UHCGame plugin) {
		this.game = plugin;
	}
	
	@EventHandler(priority = EventPriority.MONITOR)
	public void onPlayerDeath(PlayerDeathEvent e) {
		UHCPlayer player = game.getPlayer((Player)e.getEntity());
		player.reportStats(game.getUHCCore());
		game.returnPlayerToLobby(e.getEntity(), game.getLangHandler().getString("message.player.thanksForPlaying"));
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void OnPlayerKill(EntityDeathEvent e) {
		
		Entity ent = e.getEntity();
		
		if (ent instanceof Player) {
			
			Player dead = (Player) ent;
			
			Player killer;
			
			if (dead.getKiller() instanceof Player) {
				
				killer = ((Player) ent).getKiller();
//				
				UHCPlayer uhcKiller = game.getPlayer(killer);
				UHCPlayer uhcDead = game.getPlayer(dead);
				//PlayerData deadData = new PlayerData(game.getUHCCore(), new UHCPlayer((dead)));
				//PlayerData killerData = new PlayerData(game.getUHCCore(), new UHCPlayer((killer)));
//				
//				
				//deadData.addPlayerDeath();
				
				uhcDead.addDeath();
				uhcKiller.addKill();
//				
     			//killerData.addPlayerKills(1);
				
				
			}
			
			
			
			
			
		}
		
	    
		
	}
	
}
