/**
	@author natey59
 */

package com.dgpvp.natey59.uhc.game.scatter;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;

import com.dgpvp.natey59.uhc.game.UHCGame;

public class ScatterUtils {

	private static int setradius = 0;
	private static int xcoord = 0;
	private static int zcoord = 0;
	private static World world = null;
		
	static {
		UHCGame game = (UHCGame) Bukkit.getPluginManager().getPlugin("UhcGame");
		
		ScatterUtils.setradius = game.getMapRadius();
		ScatterUtils.world = UHCGame.getGameWorld();
		ScatterUtils.xcoord = 0;
		ScatterUtils.zcoord = 0;
	}

	private static void scatterPlayer(Player p, Location loc) {
		p.teleport(loc);
	}

	public static void scatterPlayersOrganised(Player[] ps) {
		int players = ps.length;
		double increment = 6.283185307179586D / players;
		for (int i = 0; i < players; i++) {
			double angle = i * increment;
			int[] coords = convertFromRadiansToBlock(ScatterUtils.setradius, angle);

			Location finalTeleport = new Location(ScatterUtils.world, 0.0D, 0.0D, 0.0D);
			finalTeleport.setX(coords[0]);
			finalTeleport.setZ(coords[1]);
			finalTeleport.setX(finalTeleport.getX() + ScatterUtils.xcoord);
			finalTeleport.setZ(finalTeleport.getZ() + ScatterUtils.zcoord);
			finalTeleport.setX(Math.round(finalTeleport.getX()) + 0.5D);
			finalTeleport.setZ(Math.round(finalTeleport.getZ()) + 0.5D);
			if (ScatterUtils.world.getChunkAt(finalTeleport).isLoaded()) {
				ScatterUtils.world.getChunkAt(finalTeleport).load(true);
			}
			//finalTeleport.setY(getSafeY(finalTeleport));
			
			//generatePoint(finalTeleport);
			
			scatterPlayer(ps[i], finalTeleport);
		}
	}
	
	
	public static List<Location> getScatterPoints(int players, int radius) {
		
		ScatterUtils.setradius = radius;
		
		List<Location> locations = new ArrayList<Location>();
		
		double increment = 6.283185307179586D / players;
		for (int i = 0; i < players; i++) {
			
			double angle = i * increment;
			int[] coords = convertFromRadiansToBlock(ScatterUtils.setradius, angle);
			Location finalTeleport = new Location(ScatterUtils.world, 0.0D, 0.0D, 0.0D);
			finalTeleport.setX(coords[0]);
			finalTeleport.setZ(coords[1]);
			finalTeleport.setX(finalTeleport.getX() + ScatterUtils.xcoord);
			finalTeleport.setZ(finalTeleport.getZ() + ScatterUtils.zcoord);
			finalTeleport.setX(Math.round(finalTeleport.getX()) + 0.5D);
			finalTeleport.setZ(Math.round(finalTeleport.getZ()) + 0.5D);
			
			//ScatterUtils.world.getChunkAt(finalTeleport).load(true);
			
			//finalTeleport.setY(getSafeY(finalTeleport));
			//ScatterEntry entry = new ScatterEntry(ps[i], finalTeleport);
			
			while (UHCGame.getBannedSpawns().contains(world.getBiome(finalTeleport.getBlockX(), finalTeleport.getBlockY()))) {
				finalTeleport = scatterPlayerRandom();
			}
			
			locations.add(finalTeleport);
			
			Bukkit.broadcastMessage("one spawn generated.");
			
			
			
			//scatterPlayer(ps[i], finalTeleport);
		}
		
		return locations;
		
	}
	
	private static Location scatterPlayerRandom() {
		Random random = new Random();

		Location finalTeleport = new Location(world, 0.0D, 0.0D, 0.0D);

		double randomAngle = random.nextDouble() * 3.141592653589793D * 2.0D;
		double newradius = setradius * random.nextDouble();
		int[] coords = convertFromRadiansToBlock(newradius, randomAngle);

		finalTeleport.setX(coords[0]);
		finalTeleport.setZ(coords[1]);

		finalTeleport.setX(finalTeleport.getX() + xcoord);
		finalTeleport.setZ(finalTeleport.getZ() + zcoord);

		finalTeleport.setX(Math.round(finalTeleport.getX()) + 0.5D);
		finalTeleport.setZ(Math.round(finalTeleport.getZ()) + 0.5D);
				
		return finalTeleport;
	}
		
//	private static int getSafeY(Location loc) {
//		return loc.getWorld().getHighestBlockYAt(loc);
//	}

	private static int[] convertFromRadiansToBlock(double radius, double angle) {
		return new int[] { (int) Math.round(radius * Math.cos(angle)),
				(int) Math.round(radius * Math.sin(angle)) };
	}
}
