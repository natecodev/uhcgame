/**
	@author natey59
*/

package com.dgpvp.natey59.uhc.game.listeners;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.inventory.ItemStack;

import com.dgpvp.natey59.uhc.game.UHCGame;
import com.dgpvp.natey59.uhc.player.UHCPlayer;

public class OnFoodEat implements Listener {

	private UHCGame game;
	
	public OnFoodEat(final UHCGame game) {
		this.game = game;
	}
	
	@EventHandler(priority = EventPriority.MONITOR)
	public void onGoldenAppleEat(final FoodLevelChangeEvent e) {
		
		if (e.getEntity() instanceof Player) {
            ItemStack food = e.getEntity().getItemInHand();
         
            if (food.getType() == Material.GOLDEN_APPLE) {
           
            	UHCPlayer player = game.getPlayer((Player)e.getEntity());
            	
            	player.addGodAppleEaten();
            	
//            	PlayerData pData = new PlayerData(game.getUHCCore(), new UHCPlayer((Player) e.getEntity()));
//            	pData.addGodApplesEaten(1);
            	
            }
        }
		
	}
	
}
