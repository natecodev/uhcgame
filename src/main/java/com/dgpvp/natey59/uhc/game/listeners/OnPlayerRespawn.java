/**
	@author natey59
*/

package com.dgpvp.natey59.uhc.game.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerRespawnEvent;

import com.dgpvp.natey59.uhc.game.UHCGame;

public class OnPlayerRespawn implements Listener {

	private UHCGame game;
	
	public OnPlayerRespawn(UHCGame game) {
		this.game = game;
	}
	
	@EventHandler
	public void onPlayerRespawn(PlayerRespawnEvent e) {
		
		game.returnPlayerToLobby(e.getPlayer(), "Thanks for playing.");
		
	}
	
}
