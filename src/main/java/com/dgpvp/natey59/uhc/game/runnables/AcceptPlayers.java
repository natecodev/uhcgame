/**
	@author natey59
*/

package com.dgpvp.natey59.uhc.game.runnables;

import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitRunnable;

import com.dgpvp.natey59.uhc.game.UHCGame;
import com.dgpvp.natey59.uhc.messenger.PlayerMessenger;
import com.dgpvp.natey59.uhc.status.GameState;

public class AcceptPlayers extends BukkitRunnable {

	private UHCGame plugin;
	
	public AcceptPlayers(UHCGame instance) {
		this.plugin = instance;
	}
	
	@SuppressWarnings("deprecation")
	public void run() {	
		
		plugin.setGameState(GameState.ACCEPTING_PLAYERS);
		
		plugin.getLogger().info(plugin.getLangHandler().getString("message.console.waitingForPlayers"));
		
		//Wait until there are enough players on.
		while (plugin.getPlayerCount() < plugin.getMinPlayers()) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		PlayerMessenger.broadcast(plugin.getLangHandler().getString("message.broadcast.minPlayersMet"));
		
		try {
			Thread.sleep(43000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		BukkitRunnable generateBorderAndSpawns = new GenerateBorder(plugin);
		
		Bukkit.getScheduler().scheduleAsyncDelayedTask(plugin, generateBorderAndSpawns, 0);
		
				
//		BukkitRunnable generateBorderAndSpawns = new GenerateBorder(plugin);
//		
//		Bukkit.getScheduler().scheduleAsyncDelayedTask(plugin, generateBorderAndSpawns, 0);
		
	}
	
}
