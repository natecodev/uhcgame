/**
	@author natey59
*/

package com.dgpvp.natey59.uhc.game.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityRegainHealthEvent;
import org.bukkit.event.entity.EntityRegainHealthEvent.RegainReason;

public class OnNaturalRegen implements Listener {
	
	@EventHandler
	public void onNaturalRegen(EntityRegainHealthEvent e) {
		
		if (e.getEntity() instanceof Player) {
			
			if (e.getRegainReason() == RegainReason.REGEN || e.getRegainReason() == RegainReason.SATIATED) {
				e.setCancelled(true);
			}
			
		}
		
	}
	
}
