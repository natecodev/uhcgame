/**
	@author natey59
*/

package com.dgpvp.natey59.uhc.game.listeners;

import java.text.MessageFormat;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerLoginEvent.Result;

import com.dgpvp.natey59.uhc.game.UHCGame;
import com.dgpvp.natey59.uhc.game.anticheat.antiafk.MovementChecker;
import com.dgpvp.natey59.uhc.player.UHCPlayer;
import com.dgpvp.natey59.uhc.sql.PlayerData;

public class OnPlayerJoin implements Listener {

	private UHCGame uhc;
	
	public OnPlayerJoin(final UHCGame uhc) {
		this.uhc = uhc;
	}
	
	@SuppressWarnings("deprecation")
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerJoin(PlayerLoginEvent event) {
				
		UHCPlayer player = new UHCPlayer(event.getPlayer());
		
		PlayerData pData = player.getPlayerData(uhc.getUHCCore());
		
		if (!uhc.getGameState().canBeJoined()) {
			
			event.setKickMessage(MessageFormat.format(uhc.getLangHandler().getString("message.kick.gameCannotBeJoined"), uhc.getGameState().getReadableName()));
			
			event.setResult(Result.KICK_OTHER);
			
			return;
		}
		
		
		if (uhc.isPremium() == true && !pData.isPremium()) {
						
			event.setKickMessage(uhc.getLangHandler().getString("message.kick.premiumOnly"));
			
			event.setResult(Result.KICK_OTHER);
						
			return;
			
		}
		
		if (uhc.getPlayerCount() >= uhc.getMaxPlayers()) {
			if (!pData.isPremium()) {
				event.setKickMessage("Lobby full.");
				
				event.setResult(Result.KICK_OTHER);
				
				return;
			} else {
				
				boolean roomMade = uhc.makeRoom();
				
				if (roomMade) {
					event.setResult(Result.ALLOWED);
				} else {
					event.setKickMessage(uhc.getLangHandler().getString("message.kick.noRoomCouldBeMade"));
					event.setResult(Result.KICK_OTHER);
				}
				
			}
			
		}
				
		if (event.getResult() == Result.ALLOWED) {
			
			
			MovementChecker afkChecker = new MovementChecker(uhc, event.getPlayer());
			
			Bukkit.getScheduler().scheduleAsyncRepeatingTask(uhc, afkChecker, 0, 4800L);
		}
		
	}
	
	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent event) {
		
		UHCPlayer player = new UHCPlayer(event.getPlayer());
		
		uhc.addPlayer(player);
	}
	
}
