/**
	@author natey59
*/

package com.dgpvp.natey59.uhc.game.runnables;

import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitRunnable;

import com.dgpvp.natey59.uhc.game.UHCGame;
import com.dgpvp.natey59.uhc.game.generation.WorldUtils;
import com.dgpvp.natey59.uhc.status.GameState;
import com.wimbli.WorldBorder.CoordXZ;

public class GenerateMap extends BukkitRunnable {

	private UHCGame plugin;
	
	private final int defaultPadding = CoordXZ.chunkToBlock(13);
	
	String fillWorld = UHCGame.getGameWorldName();
	int fillFrequency = 20;
	int fillPadding = defaultPadding;
	boolean fillForceLoad = false;
	int repeats = fillFrequency/20;
	int ticks = 20/fillFrequency;

	public GenerateMap(UHCGame instance) {
		this.plugin = instance;
	}
	
	@SuppressWarnings("deprecation")
	@Override
	public void run() {
		plugin.setGameState(GameState.GENERATING_MAP);
		
		//Generate overworld
		WorldUtils.generateWorld(plugin, UHCGame.getGameWorldName());
		
		plugin.setGameState(GameState.PROCESSING_MAP);
		
		while (UHCGame.getBannedSpawns().contains(UHCGame.getGameWorld().getBiome(0, 0))) {
			plugin.getLogger().info(plugin.getLangHandler().getString("message.console.bannedSpawnBiome"));
			WorldUtils.generateWorld(plugin, UHCGame.getGameWorldName());
		}
		
		BukkitRunnable generateSpawns = new GenerateSpawnPoints(plugin);
		
		Bukkit.getScheduler().scheduleAsyncDelayedTask(plugin, generateSpawns, 0);
	}

	
	
}
