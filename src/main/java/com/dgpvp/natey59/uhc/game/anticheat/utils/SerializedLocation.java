/**
	@author natey59
*/

package com.dgpvp.natey59.uhc.game.anticheat.utils;

import org.bukkit.Location;

public class SerializedLocation {

	private int x, z;
	
	public SerializedLocation(Location loc) {
		
		this.x = loc.getBlockX();
		this.z = loc.getBlockZ();
		
	}
	
	public SerializedLocation(int x, int z) {
		this.x = x;
		this.z = z;
	}
	
	public int getX() {
		return this.x;
	}
		
	public int getZ() {
		return this.z;
	}
	
	public boolean equals(SerializedLocation location) {
		
		if (location.getX() == this.getX() && location.getZ() == this.getZ()) {
			return true;
		} else {
			return false;
		}
	}
		
}
