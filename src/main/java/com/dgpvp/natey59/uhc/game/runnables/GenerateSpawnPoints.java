/**
	@author natey59
*/

package com.dgpvp.natey59.uhc.game.runnables;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.scheduler.BukkitRunnable;

import com.dgpvp.natey59.uhc.game.UHCGame;
import com.dgpvp.natey59.uhc.game.scatter.ScatterMap;
import com.dgpvp.natey59.uhc.game.scatter.ScatterUtils;
import com.wimbli.WorldBorder.Config;

public class GenerateSpawnPoints extends BukkitRunnable {

	private UHCGame plugin;
	
	public GenerateSpawnPoints(UHCGame plugin) {
		this.plugin = plugin;
	}
	
	@SuppressWarnings("deprecation")
	@Override
	public void run() {
		//Generate spawns
				UHCGame.scatterMap = new ScatterMap(ScatterUtils.getScatterPoints(plugin.getMaxPlayers(), plugin.getMaxMapRadius()));
				
				for (Location location : UHCGame.scatterMap.getLocations()) {
					
					Config.setBorder(UHCGame.getGameWorldName(), 10, location.getBlockX(), location.getBlockZ());
					
					Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "wb gameWorld fill");
					Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "wb fill confirm");
					
					while (Config.fillTask.valid() && Config.fillTask != null) {
						try {
							Thread.sleep(1000);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
					
				}
				
				//Generate underworld
				
				BukkitRunnable acceptPlayers = new AcceptPlayers(plugin);
				
				Bukkit.getScheduler().scheduleAsyncDelayedTask(plugin, acceptPlayers);		
	}
}
