/**
	@author natey59
*/

package com.dgpvp.natey59.uhc.game.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;

import com.dgpvp.natey59.uhc.game.UHCGame;
import com.dgpvp.natey59.uhc.player.UHCPlayer;

public class OnPlayerDamage implements Listener {

	private UHCGame game;
	
	public OnPlayerDamage(UHCGame game) {
		this.game = game;
	}
	
	@EventHandler
	public void onPlayerDamage(EntityDamageEvent e) {
		
		if (!game.getGameState().allowsDamage()) {
			e.setCancelled(true);
		} else {
			
			if (e.getEntity() instanceof Player) {
				
				UHCPlayer player = game.getPlayer((Player)e.getEntity());

				player.addDamageTaken(e.getDamage());
				
				//PlayerData pData = new PlayerData(game.getUHCCore(), new UHCPlayer((Player) e.getEntity()));
//				
     			//pData.addDamageTaken(e.getDamage());
				
			}
			
		}
		
	}
	
	@EventHandler(priority = EventPriority.MONITOR)
	public void OnPlayerDamageByEntity(EntityDamageByEntityEvent e) {
		
		if (e.getEntity() instanceof Player) {
			
			if (e.getDamager() instanceof Player) {
				
				UHCPlayer player = game.getPlayer((Player) e.getDamager());
				
				player.addDamageDealt(e.getDamage());
				
//				PlayerData pData = new PlayerData(game.getUHCCore(), new UHCPlayer((Player) e.getDamager()));
//				
//				pData.addDamageDealt(e.getDamage());
//				
			}
			
		}
		
	}
	
 }
