/**
	@author natey59
*/

package com.dgpvp.natey59.uhc.game;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Biome;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import com.dgpvp.natey59.uhc.UHC;
import com.dgpvp.natey59.uhc.bungeecord.Connector;
import com.dgpvp.natey59.uhc.game.anticheat.antiafk.AntiAFKListener;
import com.dgpvp.natey59.uhc.game.anticheat.antiafk.HealthRemover;
import com.dgpvp.natey59.uhc.game.listeners.OnFoodEat;
import com.dgpvp.natey59.uhc.game.listeners.OnNaturalRegen;
import com.dgpvp.natey59.uhc.game.listeners.OnPlayerDamage;
import com.dgpvp.natey59.uhc.game.listeners.OnPlayerDeath;
import com.dgpvp.natey59.uhc.game.listeners.OnPlayerJoin;
import com.dgpvp.natey59.uhc.game.listeners.OnPlayerQuit;
import com.dgpvp.natey59.uhc.game.listeners.OnPlayerRespawn;
import com.dgpvp.natey59.uhc.game.runnables.GenerateMap;
import com.dgpvp.natey59.uhc.game.scatter.ScatterMap;
import com.dgpvp.natey59.uhc.game.scatter.TeleportQueue;
import com.dgpvp.natey59.uhc.messenger.LanguageFileHandler;
import com.dgpvp.natey59.uhc.messenger.PlayerMessenger;
import com.dgpvp.natey59.uhc.player.UHCPlayer;
import com.dgpvp.natey59.uhc.sql.ServerData;
import com.dgpvp.natey59.uhc.status.GameState;

public class UHCGame extends JavaPlugin {
	
	private ServerData serverData;
	
	private String serverName;
	
	private UHC uhcCore;
	
	private GameState state;
	
	private boolean isPremium;
	
	private List<UHCPlayer> players;
	
	private int minPlayers = 0;
	private int maxPlayers = 0;
	
	private List<UUID> afkPlayers = new ArrayList<UUID>();
	
	private LanguageFileHandler language;
	
	public static ScatterMap scatterMap;
	
	private List<Player> disconnected = new ArrayList<Player>();
	
	private TeleportQueue teleportQueue = new TeleportQueue();
			
	@SuppressWarnings("deprecation")
	@Override
	public void onEnable() {
		
		try {
			language = new LanguageFileHandler();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		this.saveDefaultConfig();
		
		players = new ArrayList<UHCPlayer>();
		
		uhcCore = (UHC) this.getServer().getPluginManager().getPlugin("UHCCore");
		
		serverName = this.getConfig().getString("name");
		
		serverData = new ServerData(uhcCore, serverName);
		
		isPremium = serverData.getIsPremium();
		
		PluginManager pm = this.getServer().getPluginManager();
		
		pm.registerEvents(new OnPlayerJoin(this), this);
		pm.registerEvents(new OnPlayerDeath(this), this);
		pm.registerEvents(new OnPlayerDamage(this), this);
		pm.registerEvents(new OnFoodEat(this), this);
		pm.registerEvents(new OnNaturalRegen(), this);
		pm.registerEvents(new OnPlayerQuit(this), this);
		pm.registerEvents(new AntiAFKListener(this), this);
		pm.registerEvents(new OnPlayerRespawn(this), this);
		
		
		this.minPlayers = serverData.getMinPlayers();
		this.maxPlayers = serverData.getMaxPlayers();
				
		BukkitRunnable genMap = new GenerateMap(this);
				
		genMap.runTask(this);
		
		HealthRemover afkTick = new HealthRemover(this);
		
		Bukkit.getScheduler().scheduleAsyncRepeatingTask(this, afkTick, 0, 80L);
		
	}
	
	
	@Override
	public void onDisable() {
		
	}
	
	public TeleportQueue getTeleportQueue() {
		return this.teleportQueue;
	}
	
	public void addTeleport(Player player, Location location) {
		this.teleportQueue.queueTeleport(player, location);
	}
	
	public List<Player> getDisconnected() {
		return this.disconnected;
	}
	
	public int getMapRadius() {
		int radius = ((this.getPlayerCount()) * 100) /2;
		
		radius = (radius < 750) ? 750 : radius;
		
		return radius;
	}
	
	public int getMaxMapRadius() {
		
		int radius = ((this.getMaxPlayers()) * 100) /2;
		
		radius = (radius < 750) ? 750 : radius;
		
		return radius;
		
	}
	
	public LanguageFileHandler getLangHandler() {
		return this.language;
	}
	
	public void setGameState(GameState gameState) {
		this.serverData.setServerState(gameState);
		this.state = gameState;
		this.getLogger().info(MessageFormat.format(this.getLangHandler().getString("message.console.gamestateUpdate"), gameState.getReadableName()));
	}
	
	public GameState getGameState() {
		return state;
	}
	
	public void addAfkPlayer(Player player) {
		this.afkPlayers.add(player.getUniqueId());
	}
	
	public List<UUID> getAfkPlayers() {
		return this.afkPlayers;
	}
	
	public void removeAfkPlayer(Player player) {
		if (this.afkPlayers.contains(player.getUniqueId())) {
			this.afkPlayers.remove(player.getUniqueId());
		}
	}
	
	public boolean isPremium() {
		return this.isPremium;
	}
	
	public static List<Biome> getBannedSpawns() {
		ArrayList<Biome> bannedSpawns = new ArrayList<Biome>();
		
		bannedSpawns.add(Biome.BEACH);
		bannedSpawns.add(Biome.OCEAN);
		bannedSpawns.add(Biome.SWAMPLAND);
		bannedSpawns.add(Biome.SWAMPLAND_MOUNTAINS);
		bannedSpawns.add(Biome.MUSHROOM_ISLAND);
		bannedSpawns.add(Biome.MUSHROOM_SHORE);
		bannedSpawns.add(Biome.FROZEN_OCEAN);
		bannedSpawns.add(Biome.DEEP_OCEAN);
		bannedSpawns.add(Biome.FROZEN_RIVER);
		bannedSpawns.add(Biome.ICE_PLAINS_SPIKES);
		bannedSpawns.add(Biome.ICE_MOUNTAINS);
		bannedSpawns.add(Biome.BIRCH_FOREST_MOUNTAINS);
		bannedSpawns.add(Biome.BIRCH_FOREST_HILLS_MOUNTAINS);
		bannedSpawns.add(Biome.COLD_TAIGA_MOUNTAINS);
		bannedSpawns.add(Biome.COLD_BEACH);
		bannedSpawns.add(Biome.DESERT_MOUNTAINS);
		bannedSpawns.add(Biome.EXTREME_HILLS_MOUNTAINS);
		bannedSpawns.add(Biome.EXTREME_HILLS_PLUS_MOUNTAINS);
		bannedSpawns.add(Biome.MESA_BRYCE);
		bannedSpawns.add(Biome.MESA_PLATEAU);
		bannedSpawns.add(Biome.JUNGLE_EDGE_MOUNTAINS);
		bannedSpawns.add(Biome.MESA_PLATEAU_FOREST_MOUNTAINS);
		bannedSpawns.add(Biome.MESA_PLATEAU_MOUNTAINS);
		bannedSpawns.add(Biome.DESERT_MOUNTAINS);
		bannedSpawns.add(Biome.ROOFED_FOREST_MOUNTAINS);
		bannedSpawns.add(Biome.SAVANNA_MOUNTAINS);
		bannedSpawns.add(Biome.SAVANNA_PLATEAU_MOUNTAINS);
		bannedSpawns.add(Biome.SMALL_MOUNTAINS);
		bannedSpawns.add(Biome.TAIGA_MOUNTAINS);
		
		
		return bannedSpawns;
	}
	
	public ServerData getServerData() {	
		return this.serverData;
	}
	
	public UHC getUHCCore() {
		return this.uhcCore;
	}
	
	public int getPlayerCount() {
		return this.getServer().getOnlinePlayers().length;
	}
	
	public int getMinPlayers() {
		return this.minPlayers;
	}
	
	public int getMaxPlayers() {
		return this.maxPlayers;
	}
	
	public void returnPlayerToLobby(Player player, String reason) {
		
		PlayerMessenger.errorMessage(player, reason);
		
		//this.serverData.setPlayerCount(this.serverData.getPlayerCount()-1);
		
		this.removePlayer(new UHCPlayer(player));
		
		Connector.connectPlayerToServer(this.getUHCCore(), player, this.getConfig().getString("lobby"));
		
	}
	
	public void removePlayer(UHCPlayer player) {
		
		this.players.remove(player);
		
	}
	
	public void addPlayer(UHCPlayer player) {
		
		this.serverData.setPlayerCount(serverData.getPlayerCount() + 1);
		
		this.players.add(player);
		
		player.getPlayer().teleport(player.getPlayer().getWorld().getSpawnLocation());
		
		player.message(this.getLangHandler().getString("message.join.welcome"));
		
		PlayerMessenger.broadcast
			(MessageFormat.format(this.getLangHandler().getString("message.join.informAll"), new Object[] {player.getPlayer().getName(), (serverData.getMinPlayers()-this.getPlayerCount() + "")}));
		
	}
	
	public UHCPlayer getPlayer(Player player) {
		
		UHCPlayer toReturn = null;
		
		for (UHCPlayer uPlayer : this.players) {
			if (uPlayer.getPlayer().equals(player) ) {
				toReturn = uPlayer;
			}
		}
		
		return toReturn;
		
	}
	
	public void clearPlayers() {
		this.serverData.setPlayerCount(0);
		
	}
	
	public static World getGameWorld() {
		
		World gameWorld = Bukkit.getWorld("gameWorld");
		
		return gameWorld;
	}
	
	public static String getGameWorldName() {
		return "gameWorld";
	}
	
	public boolean makeRoom() {
		
		boolean roomMade = false;
		
		for (UHCPlayer player : this.players) {
			
			if (!player.getPlayerData(uhcCore).isPremium()) {			
				
				player.getPlayer().kickPlayer(MessageFormat.format(this.getLangHandler().getString("message.kick.replacedByPremium"), this.getLangHandler().getString("misc.string.store")));
				
				roomMade = true;
				
				break;
				
			}
		}
		
		return roomMade;
	}
	
}





















