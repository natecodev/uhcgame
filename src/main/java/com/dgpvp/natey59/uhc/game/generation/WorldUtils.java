/**
	@author natey59
*/

package com.dgpvp.natey59.uhc.game.generation;

import org.bukkit.World.Environment;
import org.bukkit.Bukkit;
import org.bukkit.WorldType;

import com.dgpvp.natey59.uhc.game.UHCGame;
import com.onarandombox.MultiverseCore.MultiverseCore;

public class WorldUtils {

	private WorldUtils() { }
	
	public static void deleteWorld(UHCGame plugin, String world) {
		
		plugin.getLogger().info("Deleting: " + world);
		
		MultiverseCore mvCore = (MultiverseCore) plugin.getServer().getPluginManager().getPlugin("Multiverse-Core");
		mvCore.deleteWorld(world);
		
		plugin.getLogger().info(world + " has been deleted");
	}
	
	public static void createWorld(UHCGame plugin, String world) {
		
		plugin.getLogger().info("Creating world: " + world); 
		
		MultiverseCore mvCore = (MultiverseCore) plugin.getServer().getPluginManager().getPlugin("Multiverse-Core");
		mvCore.getMVWorldManager().addWorld(world, Environment.NORMAL, "", WorldType.NORMAL, true, "TerrainControl");
				
		Bukkit.getWorld(world).setSpawnLocation(0, 72, 0);
		
		plugin.getLogger().info(world + " has been created.");
	}
	
	public static void generateWorld(UHCGame plugin, String world) {
		
		WorldUtils.deleteWorld(plugin, world);
		WorldUtils.createWorld(plugin, world);
		
	}
	
}
