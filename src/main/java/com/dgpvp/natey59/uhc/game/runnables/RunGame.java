/**
	@author natey59
*/

package com.dgpvp.natey59.uhc.game.runnables;

import java.text.MessageFormat;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import com.dgpvp.natey59.uhc.game.UHCGame;
import com.dgpvp.natey59.uhc.messenger.PlayerMessenger;
import com.dgpvp.natey59.uhc.player.UHCPlayer;
import com.dgpvp.natey59.uhc.status.GameState;

public class RunGame extends BukkitRunnable {
	
	private UHCGame plugin;
	
	private long timeElapsed = 0;
	
	private boolean timeRanOut = false;
	
	public RunGame(UHCGame instance) {
		this.plugin = instance;
	}
	
	@Override
	public void run() {		
		
		plugin.setGameState(GameState.IN_GAME);
		
		UHCGame.getGameWorld().setTime(0);
		
				
		while (plugin.getPlayerCount() > 2 && (timeElapsed < 10800)) {
			try {
				Thread.sleep(1000);
				timeElapsed++;
				
				if ((timeElapsed/60)%20 == 0) {
					
					PlayerMessenger.broadcast(MessageFormat.format(plugin.getLangHandler().getString("message.broadcast.minutesHaveElapsed"), timeElapsed/60));
					
				}
				
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		//Bukkit.broadcastMessage("2 players remaining. There will be a death match in 1 minute! Prepare!");
		
		if (timeRanOut) {
			PlayerMessenger.broadcast(plugin.getLangHandler().getString("message.broadcast.noTimeLeftDeathMatch"));
		} else {
			PlayerMessenger.broadcast(plugin.getLangHandler().getString("message.broadcast.oneMinuteUntilDeathMatch"));
		}
		
		try {
			Thread.sleep(55000);
			PlayerMessenger.broadcast("5...");
			Thread.sleep(1000);
			PlayerMessenger.broadcast("4...");
			Thread.sleep(1000);
			PlayerMessenger.broadcast("3...");
			Thread.sleep(1000);
			PlayerMessenger.broadcast("2...");
			Thread.sleep(1000);
			PlayerMessenger.broadcast("1...");
			Thread.sleep(1000);
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		plugin.setGameState(GameState.GRACE_PERIOD);
		
		for (Player player : Bukkit.getOnlinePlayers()) {
			player.teleport(new Location(Bukkit.getWorld("gameWorld"), 0, 100, 0));
		}
		
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		plugin.setGameState(GameState.DEATH_MATCH);
		
		PlayerMessenger.broadcast(plugin.getLangHandler().getString("message.broadcast.deathmatchFight"));
		
		while (plugin.getPlayerCount() > 1) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
				
		UHCPlayer winner = plugin.getPlayer(Bukkit.getOnlinePlayers()[0]);
		winner.reportStats(plugin.getUHCCore());
		
		plugin.returnPlayerToLobby(Bukkit.getOnlinePlayers()[0], plugin.getLangHandler().getString("message.player.winnerCongratulation"));
		
		plugin.clearPlayers();
		
		BukkitRunnable cleanup = new PostGameCleanup(plugin);
		
		Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, cleanup, 0);
		
	}
	
	
	
}
