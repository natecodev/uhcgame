/**
	@author natey59
*/

package com.dgpvp.natey59.uhc.game.anticheat.antiafk;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

import com.dgpvp.natey59.uhc.game.UHCGame;

public class AntiAFKListener implements Listener {

	private UHCGame game;
	
	public AntiAFKListener(UHCGame game) {
		this.game = game;
	}
	
	@EventHandler
	public void playerMoveEvent(PlayerMoveEvent e) {
		
		game.removeAfkPlayer(e.getPlayer());
		
	}
	
	
}
