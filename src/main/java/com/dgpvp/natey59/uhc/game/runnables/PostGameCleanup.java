/**
	@author natey59
*/

package com.dgpvp.natey59.uhc.game.runnables;

import java.io.File;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import com.dgpvp.natey59.uhc.game.UHCGame;
import com.dgpvp.natey59.uhc.status.GameState;

public class PostGameCleanup extends BukkitRunnable {

	private UHCGame game;
	
	public PostGameCleanup(UHCGame game) {
		this.game = game;
	}
	
	@Override
	public void run() {
		
		game.setGameState(GameState.POST_GAME);
		
		//Just in case there are some how players on the server they will be returned to the lobby
		for (Player player : Bukkit.getOnlinePlayers()) {
			game.returnPlayerToLobby(player, "I have no idea how you were still in that lobby, but goodbye.");
		}
		
		//Clean up MySQL
		game.getServerData().setPlayerCount(0);
		
		//Delete player data from World
		File playerDataFolder = new File("world/playerdata");
		
		File[] playerFiles = playerDataFolder.listFiles();
		
		for (File playerFile : playerFiles) {
			playerFile.delete();
		}
		
		File playerStatFolder = new File("world/stats");
		
		File[] playerStatFiles = playerStatFolder.listFiles();
		
		for (File playerStatFile : playerStatFiles) {
			playerStatFile.delete();
		}
		
		Bukkit.getServer().shutdown();
		
//		BukkitRunnable genMap = new GenerateMap(game);
//		
//		Bukkit.getScheduler().scheduleSyncDelayedTask(game, genMap, 0);
		
	}
	
}
